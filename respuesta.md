# Ejercicio 3. Análisis general

## ¿Cuántos paquetes componen la captura?
    La componen 1050 capturas.
## ¿Cuánto tiempo dura la captura?
    La captura dura 10'52 segundos.
## ¿Qué IP tiene la máquina donde se ha efectuado la captura?
    La IP de la máquina en la que se ha efectuado la captura, es, 192.168.1.116
## ¿Se trata de una IP pública o de una IP privada?
    Se trata de una IP privada.
## ¿Por qué lo sabes?
    Porque se trata de una dirección comprendida dentro del rango de las direcciones IP que son privadas.
    

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de Statistics. En el apartado de jerarquía de protocolos (Protocol Hierarchy) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

## ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
    UDP, RTP y SIP.

## ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
    ICM y Ethernet, que usa IPv4.

## ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
    RTP, con 134.000 bytes por segundo.

Observa por encima el flujo de tramas en el menú de Statistics en IO Graphs. La captura que estamos viendo corresponde con una llamada SIP.
Filtra por sip para conocer cuándo se envían paquetes SIP, o por 'rtp', para conocer cuándo se envían los RTP.

## ¿En qué segundos tienen lugar los dos primeros envíos SIP?
    en los segundos 0 y 0.0633
## Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
    En el paquete 6, que correspnde al segundo 0.17485
## Los paquetes RTP, ¿cada cuánto se envían?
    Cada 0.01 segundos aproximadamente.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 4. Primeras tramas
Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y que funcionará por lo tanto como UA (user agent), y "Servidor" a la máquina que proporciona el servicio SIP. Todas las pregntas de este ejercicio se refieren a la captura cuando está filtrada de forma que sólo se ven tramoas del protocolo SIP.

## ¿De qué protocolo de nivel de aplicación son?
    Son de protocolo SIP.
## ¿Cuál es la dirección IP de la máquina "Linphone"?
    192.168.1.116
## ¿Cuál es la dirección IP de la máquina "Servidor"?
    212.79.111.155
## ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
    Manda un Request con la cabecera INVITE, para establecer conexión con Servidor, usando el usuario music@sip.iptel.org
## ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?
    Servior envía a Linphone el mensaje 100 trying, que le da información, para que sepa que está intentado abrir la comunicación.

Ahora, veamos las dos tramas siguientes.

## ¿De qué protocolo de nivel de aplicación son?
    Protocolo SIP.
## ¿Entre qué máquinas se envía cada trama?
    Primero entre Servidor y Linphone, y segundo entre Linphone y Servidor.
## ¿Qué ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
    Servidor ha coonseguido establecer comunicación con Linphone, comunicándoselo con 200 OK
## ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
    Linphone manda un ACK a Servidor para avisar que le ha llegado la confirmación de la consexión por parte de Servidor.
    
Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 5. Tramas finales
Después de la trama 250, busca la primera trama SIP.

## ¿Qué número de trama es?
    La 1042
## ¿De qué máquina a qué máquina va?
    De Linphone a Servidor.
## ¿Para qué sirve?
    Para finalizar la comunicación establecida anteriormente.
## ¿Puedes localizar en ella qué versión de Linphone se está usando?
    Dentro de la captura, en el apartado SIP y en Message Header:
        User-Agent: Linphone Desktop/4.3.2 (Debian GNU/Linux bookworm/sid, Qt 5.15.6) LinphoneCore/5.0.37
    
Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 6. Invitación
Seguimos centrados en los paquetes SIP. Veamos en particular el primer paquete (INVITE)

## ¿Cuál es la dirección SIP con la que se quiere establecer una llamada?
    music@sip.iptel.org
## ¿Qué instrucciones SIP entiende el UA?
    Se puede ver las instrucciones SIP que entiende dentro de Message Header.
        Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK, UPDATE
## ¿Qué cabecera SIP indica que la información de sesión va en formato SDP?
    SIP -> Message Header -> Content-Type
    En este caso, tenemos, Content-Type: application/sdp
## ¿Cuál es el nombre de la sesión SIP?
    SIP -> Message Body -> SDP -> Session Name
    En este caso, tenemos, Session Name (s): Talk

# Ejercicio 7. Indicación de comienzo de conversación
En la propuesta SDP de Linphone puede verse un campo m con un valor que empieza por audio 7078.

## ¿Qué trama lleva esta propuesta?
    La primera trama.
## ¿Qué indica el 7078?
    Indica el puerto que habilita Linphone para recibir los datos. 
## ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
    Será donde se envien dichos paquetes.
## ¿Qué paquetes son esos?
    Paquetes RTP.

En la respuesta a esta propuesta vemos un campo m con un valor que empieza por audio XXX.

## ¿Qué trama lleva esta respuesta?
## ¿Qué valor es el XXX?
## ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
## ¿Qué paquetes son esos?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 8. Primeros paquetes RTP
Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el primer paquete RTP:

## ¿De qué máquina a qué máquina va?
    De Linphone a Servidor.
## ¿Qué tipo de datos transporta?
    Transporta datos multimedia codificados.
## ¿Qué tamaño tiene?
    124 bytes.
## ¿Cuántos bits van en la "carga de pago" (payload)?
    
## ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
    0.01 segundos aproximadamente.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

# Ejercicio 9. Flujos RTP
Vamos a ver más a fondo el intercambio RTP. Busca en el menú Telephony la opción RTP. Empecemos mirando los flujos RTP.

## ¿Cuántos flujos hay? ¿Por qué?
    Hay 2 flujos de datos, porque se está realizando una llamada.
## ¿Cuántos paquetes se pierden?
    No se pierde ningún paquete, ya que el apartado lost los dos flujos son 0%
## Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
    El valor máximo es 30.726700
## ¿Qué es lo que significa el valor de delta?
    Hace referencia a la suma de los retardos, es decir, la latencia.
## ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
    En el segundo flujo, en el de Servidor a Linphone.
## ¿Qué significan esos valores?
    Hace referencia al tiempo de retardo entre llegada de paquetes.
    

Vamos a ver ahora los valores de una trama concreta, la número 27. Vamos a analizarla en opción Telephony, RTP, RTP Stream Analysis:

## ¿Cuánto valen el delta y el jitter para ese paquete?
    Delta -> 0.000164
    Jitter -> 3.087182
## ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
    Se puede saber con el valor de skew.
## El "skew" es negativo, ¿qué quiere decir eso?
    Significa que los paquetes llegan más tarde de lo que se supone.

En el panel Stream Analysis puedes hacer play sobre los streams:

## ¿Qué se oye al pulsar play?
## ¿Qué se oye si seleccionas un Jitter Buffer de 1, y pulsas play?
## ¿A qué se debe la diferencia?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero respuesta.md (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.